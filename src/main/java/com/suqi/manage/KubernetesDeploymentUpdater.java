package com.suqi.manage;


import io.fabric8.kubernetes.api.model.Container;
import io.fabric8.kubernetes.api.model.ObjectMeta;
import io.fabric8.kubernetes.api.model.apps.Deployment;
import io.fabric8.kubernetes.client.KubernetesClient;
import io.fabric8.kubernetes.client.KubernetesClientException;
import io.fabric8.kubernetes.client.dsl.RollableScalableResource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;


@Slf4j
@Component
public class KubernetesDeploymentUpdater {

    @Autowired
    private KubernetesClient kubernetesClient;


    public String updateDeploymentImage(String namespace, String serviceName, String image) {
        List<Deployment> deploymentList;
        try {
            deploymentList = kubernetesClient.apps().deployments().inNamespace(namespace).list().getItems();
        } catch (KubernetesClientException e) {
            log.error("获取部署列表失败", e);
            return "获取部署列表失败";
        }

        Deployment deployment = findDeployment(deploymentList, serviceName);
        if (deployment == null) {
            return namespace + "命名空间中没有找到" + serviceName + "服务";
        }
        String oldImage;
        try {
            oldImage = updateContainerImage(deployment, serviceName, image);
        } catch (Exception e) {
            log.error("获取镜像失败", e);
            return "获取镜像失败";
        }

        try {
            RollableScalableResource<Deployment> resource = kubernetesClient.apps().deployments().inNamespace(namespace).resource(deployment);
            resource.forceConflicts().serverSideApply();
        } catch (KubernetesClientException e) {
            log.error("更新部署失败", e);
            return "更新部署失败";
        }

        return "正在部署...更新镜像：" + oldImage + " -> " + image;
    }

    private Deployment findDeployment(List<Deployment> deploymentList, String serviceName) {
        serviceName = serviceName + "-deployment";
        String finalServiceName = serviceName;
        return deploymentList.stream()
                .filter(deployment -> deployment.getMetadata().getName().equals(finalServiceName))
                .findFirst()
                .orElse(null);
    }

    private String updateContainerImage(Deployment deployment, String serviceName, String newImage) {
        ObjectMeta metadata = deployment.getMetadata();
        metadata.setManagedFields(null); // 根据具体需求确定是否需要设置为null

        List<Container> containers = deployment.getSpec().getTemplate().getSpec().getContainers();
        Container container = findContainer(containers, serviceName);
        if (container == null) {
            throw new RuntimeException(deployment.getMetadata().getName() + "中,没有找到" + serviceName + "镜像");
        }

        String oldImage = container.getImage();
        container.setImage(newImage);
        return oldImage;
    }

    private Container findContainer(List<Container> containers, String serviceName) {
        return containers.stream()
                .filter(container -> container.getName().equals(serviceName))
                .findFirst()
                .orElse(null);
    }
}
