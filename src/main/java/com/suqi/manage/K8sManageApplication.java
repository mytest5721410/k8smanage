package com.suqi.manage;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class K8sManageApplication {

    public static void main(String[] args) {
        SpringApplication.run(K8sManageApplication.class, args);
    }

}
