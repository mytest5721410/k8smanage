package com.suqi.manage;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.fabric8.kubernetes.client.KubernetesClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class K8sController {

    @Autowired
    private KubernetesClient kubernetesClient;

    @Autowired
    private KubernetesDeploymentUpdater kubernetesDeploymentUpdater;

    ObjectMapper objectMapper = new ObjectMapper();


    @GetMapping("getNs")
    public void ns() {
        kubernetesClient.namespaces().list().getItems().forEach(s -> {
            System.out.println(s.getMetadata().getName());
            try {
                System.out.println(objectMapper.writeValueAsString(s));
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
        });
    }

    @PostMapping("applyFile")
    public void applyFile() {
        kubernetesClient.load(this.getClass().getResourceAsStream("/test-resource-list.yaml"))
                .inNamespace("default")
                .createOrReplace();
    }

    @GetMapping("setImage")
    public String setImage(@RequestParam("ns") String namespace, @RequestParam("serviceName") String serviceName, @RequestParam("image") String image) {
        return kubernetesDeploymentUpdater.updateDeploymentImage(namespace, serviceName, image);
    }

}
