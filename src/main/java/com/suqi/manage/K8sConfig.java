package com.suqi.manage;

import io.fabric8.kubernetes.client.KubernetesClient;
import io.fabric8.kubernetes.client.KubernetesClientBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.InputStream;

@Slf4j
@Configuration
public class K8sConfig {

    @Bean
    public KubernetesClient kubernetesClient() {
        KubernetesClient client = null;
        try (InputStream inputStream = K8sManageApplication.class.getClassLoader().getResourceAsStream("k8s.conf")) {
            client = new KubernetesClientBuilder().withConfig(inputStream).build();
        } catch (Exception e) {
            log.error("", e);
        }
        return client;
    }

}
